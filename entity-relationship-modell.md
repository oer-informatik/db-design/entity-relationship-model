## Modellierung einer Datenbank: Entity-Relationship-Modell

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/110195942328913323</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/erm</span>


> **tl/dr;** _(ca. 20 min Lesezeit): Im konzeptuellen Modell wird die innere Struktur der Daten erfasst, die in einer Datenbank persistiert werden sollen. Auf Basis des so erstellten ER-Modells kann später entschieden werden, welches Datenmodell zur Speicherung der vorliegenden Struktur am geeignetsten ist._

### Konzeptueller Datenbankentwurf

Beim Entwurf einer Datenbank werden in unterschiedlichen Phasen die Informationen, die gespeichert werden sollen, abstrahiert, gegliedert und zur Speicherung strukturiert. In der ersten Phase - dem konzeptuellen Entwurf - wird dabei die Frage, wie die Daten später gespeichert werden sollen, bewusst offen gelassen.

![Die Entwurfsphasen einer Datenbank vom konzeptuellen über den logischen zum physischen Entwurf](images/datenbank-entwurfsphasen.png)

In einem konzeptuellen Datenbankentwurf werden funktionale und informationelle Anforderungen der Use-Cases an ein Datenbanksystem erfasst und analysiert.

In den Blick werden hierbei lediglich die Erfordernisse der Fachdomäne genommen, Implementierungsdetails spielen in dieser Phase noch keine Rolle.

Das konzeptuelle Datenmodell enthält die Daten, die erforderlich sind, um die benötigten Informationen der aktuellen (und vorhersehbar zukünftigen) Use Cases bereitzustellen.

Ein konzeptuelles Datenmodell:

* beschreibt, welche Daten aus Sicht des Use-Cases / Geschäftsprozesses erforderlich sind,

* erleichtert die Diskussion unter den Beteiligten (Kunde, Fachdomäne, Management, Entwickler) im Projekt,

* hilft dabei, Fehler und Missverständnisse zu vermeiden,

* dokumentiert die abstrakte Datenschicht als Zielvorstellung des Kunden und

* definiert eine gemeinsame Sprache im Projektumfeld, auf die im physischen Entwurf zurückgegriffen werden kann.

### Ziele der Entity-Relationship-Modellierung:

* Erfassen aller benötigten Informationen und zugrunde liegenden Daten

* Jede Information nur an einer Stelle modellieren (keine Dopplungen).

* Keine abgeleiteten Attribute modellieren, sondern Beziehungen zu ursprünglichen Attributen herstellen

* Anordnung der Attribute und Entitätstypen in naheliegender, vorhersehbarer und logischer Ordnung.

### Konzeptueller Entwurf einer Datenbank: Das _Entity Relationship Model_ nach Chen

Das Entity-Relationship-Modell stellt eine Beschreibung der Struktur der Datenschicht eines Projekts dar. Diese Repräsentationsform ist implementierungsfrei - d.h., es kann sich bei der späteren Datenschicht noch um jeden physischen Datenbanktypen handeln. Wenn also noch nicht von Anfang an fest steht, dass die Daten in ein relationales Datenmodell (Tabellen) eingepflegt werden sollen, sollte man die Struktur möglichst offen halten und nicht bereits in Tabellen denken. 

Die Entscheidung, welches Datenmodell gewählt wird, fällt erst in der folgenden Implementierungsphase. Im Entity-Relationship-Modell ist offen gehalten, welches Datenmodell das geeignetste für die Problemstellung ist. Denkbar sind zum Beispiel:

* schemafreie oder teilstrukturierte Daten (z.B. DocumentStore), 

* Key-Value-Paare, 

* Dateien im YAML oder JSON-Format,

* XML-Dateien, 

* Graph-Datenbank,

* Wide-Column-Datenbanken 

* oder hierarchische Struktur (z.B. Dateisystem).


Die häufigste Repräsentanz des Entity-Relationship-Modells stellen ER-Diagramme dar, die in unterschiedlichen Notationen dokumentiert werden. In der Modellierungsphase zu Beginn wird häufig auf die Chen-Notation zurückgegriffen, die auf den Informatiker Peter Pin-Shan Chen zurückgeht (siehe links unten):

![ER-Diagramm mit den Entitätstypen Klasse und Schüler*in in Chen-Notation](images/erd-chen-einfach.png)


#### Entitätstyp

Ein Entitätstyp bildet Dinge der realen Welt ab (Kunden, Artikel, Gegenstände, Rechnungen, Aufträge usw.). Ein Entitätstyp verfügt über Eigenschaften (die Attribute, s.u.). 

Die Nomen in Anforderungsdokumenten sind Kandidaten für spätere Entitätstypen, wenn ihnen weitere Eigenschaften zugewiesen werden können.

Die Objekte der realen Welt, die dadurch modelliert werden, sind in Abgrenzung zu Entitäts_typen_ einfach _Entitäten_ (beides wird im englischen häufig einfach _entity_ genannt, eigentlich spricht Chen von _entity set_ als Entitätstyp). 

![ER-Diagramm mit den hervorgehobenen Entitätstypen Klasse und Schüler*in in Chen-Notation](images/erd-chen-einfach-entitaetstyp.png)

Entitätstypen werden in der Chen-Notation als Rechtecke modelliert wie oben im Bild `Klasse` (eine _Entität_ dazu wäre z.B. die konkrete Klasse `1a`) und `Schüler*in` (_Entität_ z.B. `Peter Mustermann`).

#### Attribut

Attribute sind Eigenschaften von Entitätstypen (Name, Anzahl, Größe, Farbe, Lagerbestand, laufende Nummer …). Attribute finden sich in den Anforderungsdokumenten in Adverbien, Adjektiven und Nomen, denen keine weitere Eigenschaft zugewiesen werden. Sie werden wie folgt gekennzeichnet: 

* Attribute sind in der Regel einwertig (enthalten einen einzigen Wert). 

* Attribute können greifbar (_tangible_) oder virtuell sein. 

* Die vergebenen Attributwerte können permanent identisch sein (z.B. Geburtsdatum) oder unbeständig (volatile) (z.B. Alter), wobei nicht veränderbare Werte vorzuziehen sind.

* Attribute können optional oder verpflichtend (_mandatory_) Werte enthalten.

* Attribute, die eine Entität identifizieren, können als Schlüsselattribute unterstrichen werden.

![ER-Diagramm mit den hervorgehobenen Attributen Bezeichnung, Klassenlehrer*in, Vorname, Nachname in Chen-Notation](images/erd-chen-einfach-attribut.png)


* Attribute werden im ER-Diagramm nach Chen als Ellipse dargestellt (wie oben `Bezeichnung`, `Klassenlehrer*in`, `Vorname`, `Nachname`)

#### Beziehungstyp

Entitätstypen sind bidirektional durch Beziehungstypen miteinander verbunden. Beziehungstypen haben zwei Eigenschaften:

* Die Kardinalität (Anzahl) werden mit Zahlen bzw. Buchstaben angegeben (1, C, N, M). Das liest sich im obigen Beispiel so: Jeder Kunde kann mehrere (N) Artikel kaufen. Jeder Artikel kann nur von einem (1) Kunden gekauft werden.

* Optionalität: Wie Attribute auch können Beziehungstypen optional oder verpflichtend (_mandatory_) sein.

* Zusätzlich können Beziehungstypen Namen tragen: in jede Richtung (an jedem Ende) einen.


![ER-Diagramm mit den hervorgehobener Beziehung in Chen-Notation](images/erd-chen-einfach-relationship.png)

Kardinalitäten gemäß einfacher Chen-Notation:

Kardinalitäten beschreiben, wie viele konkrete Entitäten des Typs1 mit anderen Entitäten verknüpft sind.

* 1:1 – Jedem Objekt ist genau 1 Objekt zugeordnet 

* 1:N – Einem Objekt ist ein oder mehrere Objekte zugeordnet 

* N:M – Einem oder mehreren Objekten ist eines oder mehrere Objekte zugeordnet 

Darüber hinaus gibt es die erweiterte Chen-Notation:

* 1:C – Dem Objekt ist ein oder kein Objekt zugeordnet (optional)

* 1:NC – Einem Objekt ist kein, ein oder sind mehrere Objekte zugeordnet (optional)

* alle weiteren Kombinationen sind natürlich auch möglich: N:M C usw.


### Erweiterte Notationsmittel für ein Entity-Relationship-Modell nach Chen

![ER-Diagramm nach Chen mit den Entitätstypen Bestellung, Artikel und Bestellposition](images/erd-komplex.PNG)


#### Schlüsselattribute

Schlüsselattribute sind Attribute (oder Attributgruppen), mithilfe derer jede einzelne konkrete Entität eines Entitätstyps eindeutig identifizierbar ist. Wenn Attributgruppen zur Identifizierung nötig sind spricht man von zusammengesetzten Schlüsseln, die aus Teilschlüsseln bestehen. Es werden dann alle Teilschlüssel markiert.

Schlüsselattribute können - wenn bereits bekannt - im Entity-Relationship-Modell benannt werden, sind in dieser Entwurfsphase aber eigentlich noch nicht nötig.

Im ER-Diagramm werden (Teil-)Schlüssel mit unterstrichenen Namen notiert:	 

![Das unterstrichene Schlüsselattribut "ArtikelNummer"](images/schluesselattribut.png)

#### Mehrwertige Attribute

In der Modellierung sind oft Attribute noch nicht einwertig. Manche Attribute enthalten Listen (mehrere gleichartige Eigenschaften, z.B. "Telefonnummern") oder Sammlungen (unterschiedliche Sammlungen, z.B. "Adresse" als Straße, Hausnummer, PLZ, Ort). Sie werden mit einer doppelten Ellipse notiert.

![Das mehrwertige Attribut "Adresse" in doppelter Ellipse](images/mehrwertiges-attribut.png)
 

#### Abgeleitete Attribute

Attribute, die sich aus anderen Attributen berechnen/ableiten lassen, werden gesondert gekennzeichnet. Beispiele hierfür sind "Bruttosumme" aus "Nettosumme", "Alter" aus "Geburtsdatum", früher auch "Anrede" aus "Geschlecht".

Abgeleitete Attribute werden als gestrichelte Ellipse notiert.

![Das abgeleitete Attribut "Auftragswert brutto" in gestrichelter Ellipse](images/abgeleitetes-attribut.png)


#### Schwache Entitätstypen und identifizierende Relation

Es gibt Entitätstypen, die in Ihrer Existenz von einem anderen Entitätstypen abhängen. Ein Rechnungsposten kann beispielsweise niemals ohne eine zugehörige Rechnung bestehen. Man bezeichnet solche nicht eigenständigen Entitätstypen als "schwache Entitätstypen" und notiert sie mit einer doppelten Linie.

![Ein schwacher Entitätstyp "Rechnungsposten", der von "Rechnung" abhängt](images/schwacher-entitaetstyp.png)


Die Beziehung eines schwachen Entitätstyps zum zugehörigen Entitätstyp ist besonders stark. Daher wird sowohl die Linie als auch die Raute ebenfalls mit einer doppelten Linie notiert. Man bezeichnet die Beziehung als identifizierende Relation.

![Die identifizierende Relation "enthält" zwischen "Rechnung" und "Rechnungsposten"](images/identifizierende-relation.png)

Sie können auch als Relationstyp zusammengefasst dargestellt werden:

![Der schwache Relationstyp "Bestellposition" zwischen "Artikel" und "Bestellung"](images/schwacher-relationstyp.png)

#### Schwacher Schlüssel

Zur Identifizierung einer konkreten Entität dienen häufig deren Attribute bzw. Attributgruppen. Daneben gibt es aber auch Fälle, in denen die Relation zu einem anderen Entitätstyp zur Identifizierung verwendet wird. Erfasst eine Stadt beispielsweise die Klassen aller öffentlichen Schulen, so ist zur Identifizierung einer konkreten Klasse neben dem Klassennamen auch noch die Zuordnung zur Schule wichtig. Das Attribut "KlassenName" kann also nur zur Identifizierung genutzt werden, wenn die identifizierende Relation zu "Schule" bekannt ist. Man spricht in diesem Fall von einem _schwachen Schlüsselattribut_, dass nur einen Teil der nötigen Information bereitstellt. Dieses wird mit einer gestrichelten Linie unterstrichen.

![Der schwache Schlüssel "Klassenname" des Entitätstyps "Klasse" neben der identifizierenden Relation zu "Schule"](images/schwacher-schluessel.png)

### Modellierungsphasen im konzeptuellen Modell

Datenbanken lassen sich erst modellieren, wenn die Anforderungen des Projekts klar beschrieben sind. Für jeden Use-Case eines Datenbankprojekts müssen die nötigen Datenbankstrukturen extrahiert und modelliert werden. Hierzu können eine Reihe von Techniken verwendet werden, die häufig nicht explizit, sondern parallel durchgeführt werden. 

#### Nominalextraktion

Wenn Anforderungen in Textdokumenten vorliegen oder sich diese aus Gesprächen entwickeln lassen, können diese genutzt werden, um per Nominalextraktion erste Erkenntnisse zur Datenbankstruktur zu gewinnen:

- Alle Nomen im Text sind Kandidaten für Entitätstypen. Welche Nomen sind zentral für den Anwendungsfall? Häufig werden Synonyme verwendet, diese sollten in einer Liste / einem Glossar verzeichnet werden und eine einheitliche Bezeichnung gewählt werden. Solche Listen schaffen eine gemeinsame Sprache im Projekt und helfen dabei, später Missverständnisse zu vermeiden. Wo ist die Bedeutung der Wörter tatsächlich identisch? Worin unterscheiden sie sich? Ggf. stehen die ähnlich klingende Wörter für denselben Entitätstyp jedoch mit unterschiedlichen Attributen.

- Alle Adverbien und Adjektive sind Kandidaten für Attribute. Wenn Adverbien oder Adjektive klar einem oben analysierten Nomen zu geordnet werden können, dann stellen diese Attribute dieses Entitätstyps dar.

- Nomen, die Eigenschaften anderer Nomen beschreiben und von denen selbst keine Untereigenschaften analysiert wurden, sind ebenfalls Kandidaten für Attribute.

- Relationen zwischen Entitätstypen finden sich häufig in Verben oder Nomen, die diese Beziehungen beschreiben.

#### Bottom-Up-Entwurf per Generalisierung

Aus der Nominalextraktion werden eine Reihe von teils unverknüpften Entitätstypen und Attributen gewonnen, die mit folgenden Schritten verfeinert werden kann:

- Welche Attribute ohne Entitätstypen (und umgekehrt) wurden ermittelt? Lässt sich ein zugehöriger Entitätstyp oder weitere Attribute finden? Falls nicht: ist dieses Element wirklich für den Anwendungsfall relevant (oder ggf. bereits an anderer Stelle modelliert)?

- Wo bestehen noch relevante Beziehungen zwischen Entitätstypen? Sind alle notierten Beziehungen für den Anwendungsfall relevant? Ggf. müssen neue Relationstypen einführt, umbenannt oder entfernt werden.

- Zu welchen Entitätstypen lassen sich Oberbegriffe finden? Stellen die Oberbegriffe eigene Entitätstypen dar, denen ggf. ein Teil der Attribute zugeordnet werden kann? So lassen sich neue strukturgebende Entitätstypen finden.

- Wo lassen sich aus Attributen neue Entitätstypen mit weiteren Eigenschaften extrahieren?

So wird aus einer Sammlung von unverknüpften Attributen und Entitätstypen schrittweise eine zusammenhängende Struktur.

#### Top-Down-Entwurf per Spezialisierung

Der Top-Down-Entwurf geht den umgekehrten Weg: 

- Ausgangspunkt sind Oberbegriffe: Was sind die zentralen Begriffe des Anwendungsfalls?

- Welche Spezialisierungen / Unterbegriffe ergeben sich daraus?

- In welche Entitätstypen können die einzelnen Unterbegriffe aufgeteilt werden?

- In welchen Beziehungen stehen diese Entitätstypen zueinander?

- Welche Attribute haben diese Entitätstypen?

- Aus welchen Attributen können weitere Entitätstypen gewonnen werden?

Die letzten Schritte wiederholen sich.

#### Inside-Out-Entwurf

Wenn bereits Teile einer Datenbank bestehen - oder ein Projekt inkrementell entwickelt wird - wird häufig ein bestehendes Konstrukt erweitert. Die genutzten Techniken entsprechen einer Mischung aus Top-Down und Bottum-Up-Entwurf, da die bestehende Struktur sowohl generalisiert als auch spezialisiert werden kann.

#### Extraktion von Relationstypen per Matrix

Gerade bei komplexen Datenbanken ist es manchmal nicht trivial, die einzelnen Beziehungen von Entitätstypen zu ermitteln. Hier kann es helfen, die einzelnen Entitätstypen als Matrix aufzuzeichnen (jeweils als Spalte und als Zeile) und zwischen allen Kombinationen Bezeichnungen zu finden. Wichtig ist, stets in der gleichen Richtung zu lesen (zeilenweise): _Kunde_ bestellt _Artikel_ (nicht umgekehrt).


| | Kunde | Rechnung | Artikel | Lieferung| Adresse|
|---|---|---|---|---|---|
| **Kunde** |empfielt||bestellt<br/>bewertet<br/>fügt auf Merksliste|erhält|...|
| **Rechnung** |wird bezahlt von|korrigiert|...|...|wird gesendet an|
| **Artikel** |...|...|-|...|...|
| **Lieferung**|...|...|enthält|-|wird geliefert an|
| **Adresse**|ist Lieferadresse von<br/>ist Rechnungsadresse von<br/>|...|...|...|-|

Häufig haben identische Entitätstypen keine Beziehung zueinander (z.B. Adresse mit Adresse), manchmal lassen sich selbstreferenzielle Beziehungen finden, die aber nicht im Anwendungsfall relevant sein müssen (z.B. Kunde empfiehlt Kunde). Aus manchen Beziehungen lassen sich neue Entitätstypen ableiten (z.B. "Bestellung"). Einige Beziehungen lassen sich später auflösen, da sie nicht direkt, sondern durch weitere Entittätstypen hindurch realisiert werden (Artikel - Lieferung - Kunde).

#### Extraktion von Kardinalitäten und Optionalitäten per ERDish

Im Oracle-Datenbankkurs wird eine textuelle Repräsentanz eines ER-Modell eingeführt, die aus meiner sich hilft, Kardinalitäten und Optionalitäten zu bestimmen. Für jeden Relationstyp werden zwei Sätze gebildet, die die Beziehung in eine Richtung beschreiben, beispielsweise für den oben genannten Zusammenhand zwischen Adresse und Rechnung:

||Entitätstyp A|Optionalität|Beziehung|Kardinalität|Entitätstyp B|
|---|---|---|---|---|---|
|Each| _invoice_ |`MUST`| _be sent to_ |`ONE AND ONLY ONE` |_address_.|
|Each| _address_ |`MAY` |_be the receipient of_ |`ONE OR MORE` |_invoices_.|

Aufgrund des Satzbaus funktioniert das im Englischen etwas schöner als im Deutschen. Wichtig ist, dass für jede Beziehung genau zwei Sätze formuliert werden. Die Optionalität findet sich im ER-Diagramm häufig versteckt in der Kardinalität ("MC" statt "M") oder wird mit gestrichtelten Linien eingezeichnet.

Wer IHK-Prüfungsaufgaben aufmerksam liest wird feststellen, dass diese häufig "auf ERDish" formuliert sind. Nicht immer sind dort allerdings beide Richtungen beschrieben, sodass ggf. das Modell ergänzt werden muss.

#### Analogie Objektorientierte Programmierung zu ER-Modell

Die Begrifflichkeiten des Entity-Relationship-Modells haben jeweils Analogien in der Objektorientierten Programmierung

|Tabelle|Entity-Relationship-Modell (ERM)|Objektorientierte Programmierung (OOP)|Modellierung|
|---|---|---|---|
|Kopfzeile|Entitätstyp|Klasse|Nomen mit Untereigenschaften|
||Spalte|Attribut|Attribut|Adjektive, Adverbien, ggf. auch Nomen ohne Untereigenschaften
|Inhalt|Entitätsmenge|Objektmenge||
|Verben|	|Beziehung|Assoziation|
||Zeile|Entität|Objekt||
|Zelle|Attributwert|Attributwert||

### Notation der Kardinalitäten des ER-Modells nach Martin ("Krähenfuß-Notation" / "_crow's foot_") und Chen

Neben der Chen-Notation ist die Notation nach Martin vor allem in Modellierungstools verbreitet. Wegen ihres charakteristischen Aussehens wird sie oft als "Krähenfußnotation" bezeichnet.

Analog zur Chen Notation lassen sich 1:1, 1:N und N:M Kardinalitäten darstellen. Die Attribute werden hier von Tools oft als einfache Liste im Segment unterhalb des Entitätstypnamens gelistet (analog zum UML-Klassendiagramm). 

Nach Martin-Notation wird die Kardinalität auf Seiten der "1" mit einem einfachen Strich, auf Seiten "N" mit einer Gabelung dargestellt (letztere gibt der Krähenfußnotation den Namen). Bei einer einfachen 1:N-Beziehung sehen die beiden Varianten nach Martin (unten) und Chen (oben) folgendermaßen aus:

![ERD nach Martin und Chen für eine 1:N Beziehung zwischen Waggon und Lok](images/waggon-lok-1-n-martin-chen.webp)

Eine N:M-Beziehung lasst sich mit Martin (unten) durch zwei Krähenfüße modellieren:

![ERD nach Martin und Chen für eine N:M Beziehung zwischen Bahnhof und Zuglinie](images/bahnhof-zuglinie-n-m-martin-chen.webp)

Entsprechend der Notation bei 1:N können auch 1:1 Beziehungen durch jeweils einfache Striche modelliert werden.

![ERD nach Martin und Chen für eine N:M Beziehung zwischen Bahnhof und Zuglinie](images/einwohner-personalausweis-1-1-martin-chen.webp)

Die Martin Notation bietet darüberhinaus die Möglichkeit, über die Angabe von Ober- und Untergrenzen die Optionalität darzustellen. Je nach dem, ob eine Beziehung optional ist oder nicht wird an den Enden eine "0" für Optional oder der Strich für zwingend angegeben. So wird symbolisiert, ob die Beziehung zu mindestens einem Entitätstypen auf der jeweiligen Seite bestehen muss:

In diesem Fall ist die Beziehung in beiden Richtungen optional: weder muss eine Lok in einer Beziehung zu einem Waggon stehen noch umgekehrt:

![ERD nach Martin und Chen für eine 1:N Beziehung zwischen Waggon und Lok](images/waggon-lok-c-nc-martin-chen.png)

Hier gibt es Bahnhöfe, die von keinem Zug angefahren werden, jeder Zug muss aber einen Bahnhof anfahren:

![ERD nach Martin und Chen für eine N:M Beziehung zwischen Bahnhof und Zuglinie](images/bahnhof-zuglinie-n-mc-martin-chen.webp)

Jeder Personalausweis gehört zwingend zu einem Einwohner, Einwohner müssen jedoch nicht unbedingt einen Personalausweis besitzen:

![ERD nach Martin und Chen für eine N:M Beziehung zwischen Bahnhof und Zuglinie](images/einwohner-personalausweis-1-c-martin-chen.webp)


Die Martin-Notation verfügt über weniger Notationsmittel als die Chen-Notation, vor allem weil häufig die Attribute nicht mithilfe der Ellipsen, sondern Tabellarisch dargestellt werden: schwache Entitätstypen (schwache Schlüssel), abgeleitete Attribute, mehrwertige Attribute usw. lassen sich damit nicht modellieren. Dafür ist die Martin-Notation kompakter und damit übersichtlicher als die Chen-Notation. Häufig wird sie deswegen von Tools genutzt, die visuell Kartinalitäten darstellen wollen.

### Nächste Entwurfsphase: das logische Datenmodell

Nach dem Entwurf des konzeptuellen Entity-Relationship-Modells folgt das logische Datenmodell. Im Fall von relationalen Datenbanken wird dies meist mit dem Relationenmodell umgesetzt. Welche Eigenschaften das Relationenmodell hat und wie die Transformation aus dem ER-Modell erfolgt ist in einem [weiteren Blogpost beschrieben](https://oer-informatik.de/relationenmodell).

### Links und weitere Informationen

- Ausgangsdokument der ER-Modellierung: [Peter Pin-Shan Chan: "The Entity-Relationship Model: Toward a unified view of Data" (März 1977)](https://dspace.mit.edu/bitstream/handle/1721.1/47432/entityrelationshx00chen.pdf)

